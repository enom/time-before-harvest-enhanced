# Time Before Harvest Enhanced

A simple mod that shows the remaining days until a given crop can be harvested. This is a recompiled version of the [Time Before Harvest](https://www.nexusmods.com/stardewvalley/mods/5764) mod for Stardew Valley which fixes the SMAPI error and incorrect times on crops that regrow.

The default keybinding is `F2` but that can be changed in the `config.json` found in the *TimeBeforeHarvestEnhanced* mod folder. You can find the button code values for the `TriggerButton` here: https://stardewvalleywiki.com/Modding:Player_Guide/Key_Bindings

![Time Before Harvest Enhanced preview screenshot](https://gitlab.com/enom/time-before-harvest-fixed/-/raw/main/screenshot.png)
